#![doc = include_str!("../README.MD")]
#![warn(missing_docs)]
#![deny(warnings)]

#[cfg(feature = "json")]
use bytes::Buf;
use std::sync::Arc;

type Result<T> = std::result::Result<T, Error>;

// Structures

#[derive(thiserror::Error, Debug)]
#[allow(missing_docs)]
pub enum Error
{
  #[error("Future was cancelled.")]
  CancelledFuture(String),
  #[error("An error occured in the MQTT channel {0}.")]
  MqttChannelError(#[from] mqtt_channel::Error),
  #[error("Channel was closed for topic {0}.")]
  ChannelClosed(String),
  #[error("Send error when broadcasting message {0}")]
  AsyncBroadcastSend(#[from] async_broadcast::SendError<mqtt_channel::RawMessage>),
  #[error("Recv error when broadcasting message {0}")]
  AsyncBroadcastRecv(#[from] async_broadcast::RecvError),
  #[cfg(feature = "json")]
  #[error("Error during json serialization {0}")]
  JsonSerialisationError(#[from] serde_json::Error),
}

/// Handle to the connection
#[derive(Clone)]
pub struct Client
{
  client: mqtt_channel::Client,
}

impl Client
{
  /// Create a new MQTT Client, with the given name to connect at the given and MQTT broker
  pub fn new(client: mqtt_channel::Client) -> Client
  {
    Client { client }
  }
  /// Implement call service using the MQTT Request-Response pattern
  pub async fn call_raw_service(
    self,
    topic: impl Into<String>,
    message: impl Into<bytes::Bytes>,
    content_type: Option<String>,
  ) -> Result<bytes::Bytes>
  {
    let qos = rumqttc::v5::mqttbytes::QoS::ExactlyOnce;
    let topic_string = topic.into();
    let correlation_data = bytes::Bytes::copy_from_slice(uuid::Uuid::new_v4().as_bytes());
    let response_topic = format!(
      "{}/response/{}",
      topic_string,
      uuid::Uuid::new_v4().to_string()
    );
    let client = self.client.clone();
    let mut receiver_channel = client
      .clone()
      .get_or_create_raw_subscription(&response_topic, qos, 5)
      .await?;
    client
      .publish_raw(
        topic_string,
        qos,
        true,
        message,
        Some(rumqttc::v5::mqttbytes::v5::PublishProperties {
          content_type,
          response_topic: Some(response_topic),
          correlation_data: Some(correlation_data.clone()),
          ..Default::default()
        }),
      )
      .await?;
    loop
    {
      let msg = receiver_channel.recv().await?;
      if let Some(properties) = msg.properties
      {
        if let Some(received_correlation_data) = properties.correlation_data
        {
          if received_correlation_data == correlation_data
          {
            return Ok(msg.payload);
          }
        }
      }
    }
  }

  /// Call a service using json enconding
  #[cfg(feature = "json")]
  pub fn call_json_service<TRequest, TResponse>(
    self,
    topic: impl Into<String>,
    request: &TRequest,
  ) -> Result<impl std::future::Future<Output = Result<TResponse>>>
  where
    TRequest: serde::Serialize,
    for<'de> TResponse: serde::Deserialize<'de>,
  {
    let serialized = serde_json::to_string(&request)?;
    Ok(async {
      let data = self
        .call_raw_service(topic, serialized, Some("application/json".to_string()))
        .await?;
      Ok(serde_json::from_slice::<TResponse>(data.chunk())?)
    })
  }

  /// Create a new service on the topic, the handler will be called
  /// `cap` is the capacity of the input channel
  pub async fn create_raw_service(
    self,
    topic: impl Into<String>,
    handler: Box<dyn Send + Sync + Fn(bytes::Bytes) -> (bytes::Bytes, Option<String>)>,
    cap: usize,
  ) -> Result<()>
  {
    let handler = Arc::new(handler);
    let qos = rumqttc::v5::mqttbytes::QoS::ExactlyOnce;
    let topic = topic.into();
    let client = self.client.clone();

    let mut receiver = self
      .client
      .clone()
      .create_raw_subscription(&topic, qos, cap)
      .await?;
    loop
    {
      let msg = receiver.recv().await?;
      if let Some(properties) = msg.properties
      {
        if let Some(correlation_data) = properties.correlation_data
        {
          if let Some(response_topic) = properties.response_topic
          {
            let (payload, content_type) = handler(msg.payload);
            if let Err(e) = client
              .clone()
              .publish_raw(
                response_topic,
                qos,
                true,
                payload,
                Some(rumqttc::v5::mqttbytes::v5::PublishProperties {
                  content_type,
                  correlation_data: Some(correlation_data.clone()),
                  ..Default::default()
                }),
              )
              .await
            {
              log::error!(
                "Error occured when publishing answer to service call: {:?}",
                e
              );
            }
          }
          else
          {
            log::error!("Received message without response topic for service {topic:?}.");
          }
        }
        else
        {
          log::error!("Received message without correlation data for service {topic:?}.");
        }
      }
      else
      {
        log::error!("Received message without properties for service {topic:?}.");
      }
    }
  }

  /// Create a new service on the topic, the handler will be called and the data is serialized with json
  #[cfg(feature = "json")]
  pub fn create_json_service<TRequest, TResponse>(
    self,
    topic: impl Into<String>,
    handler: Box<dyn Send + Sync + Fn(&TRequest) -> TResponse>,
    cap: usize,
  ) -> impl std::future::Future<Output = Result<()>>
  where
    TResponse: serde::Serialize + 'static,
    for<'de> TRequest: serde::Deserialize<'de> + 'static,
  {
    self.create_raw_service(
      topic,
      Box::new(move |data: bytes::Bytes| {
        let request = serde_json::from_slice::<TRequest>(data.chunk());
        match request
        {
          Ok(request) =>
          {
            let resp = handler(&request);

            let json_data = serde_json::to_string(&resp);
            match json_data
            {
              Ok(json_data) => (json_data.into(), Some("application/json".to_string())),
              Err(err) =>
              {
                log::error!("Error during serialization {err:?}");
                (bytes::Bytes::new(), None)
              }
            }
          }
          Err(err) =>
          {
            log::error!("Error during deserialization {err:?}.");
            (bytes::Bytes::new(), None)
          }
        }
      }),
      cap,
    )
  }
}

#[cfg(test)]
mod tests
{
  use super::Client;
  use bytes::Buf;
  use futures::FutureExt;
  use std::env;
  #[derive(serde::Serialize, serde::Deserialize)]
  struct Request
  {
    a: f32,
    b: f32,
  }
  #[derive(serde::Serialize, serde::Deserialize)]
  struct Response
  {
    r: f32,
  }
  #[test]
  fn test_raw_services()
  {
    let rt = tokio::runtime::Runtime::new().unwrap();
    let (connection, task) = mqtt_channel::Client::build(
      "name-of-the-client-raw",
      env::var("MQTT_SERVICE_MQTT_SERVER_HOSTNAME").unwrap_or("localhost".to_string()),
      1883,
    )
    .start();
    let connection = Client::new(connection);
    rt.spawn(task);
    rt.spawn(
      connection
        .clone()
        .create_raw_service(
          "mqtt-service/test_raw/addition",
          Box::new(|data| {
            let request = serde_json::from_slice::<Request>(data.chunk()).unwrap();
            let json = serde_json::to_string(&Response {
              r: request.a + request.b,
            })
            .unwrap()
            .into();
            (json, Some("application/json".to_string()))
          }),
          10,
        )
        .map(|r| r.unwrap()),
    );
    let fut = connection.call_raw_service(
      "mqtt-service/test_raw/addition",
      serde_json::to_string(&Request { a: 1.0, b: 2.0 }).unwrap(),
      Some("application/json".to_string()),
    );

    let data = rt.block_on(fut).unwrap();
    let res = serde_json::from_slice::<Response>(data.chunk()).unwrap();
    assert_eq!(res.r, 3.0);
  }
  #[test]
  #[cfg(feature = "json")]
  fn test_json_services()
  {
    let rt = tokio::runtime::Runtime::new().unwrap();
    let (connection, task) = mqtt_channel::Client::build(
      "name-of-the-client-json",
      env::var("MQTT_SERVICE_MQTT_SERVER_HOSTNAME").unwrap_or("localhost".to_string()),
      1883,
    )
    .start();
    let connection = Client::new(connection);
    rt.spawn(task);
    rt.spawn(
      connection
        .clone()
        .create_json_service(
          "mqtt-service/test_json/addition",
          Box::new(|request: &Request| Response {
            r: request.a + request.b,
          }),
          10,
        )
        .map(|r| r.unwrap()),
    );
    let fut = connection
      .call_json_service::<Request, Response>(
        "mqtt-service/test_json/addition",
        &Request { a: 1.0, b: 2.0 },
      )
      .unwrap();

    let res = rt.block_on(fut).unwrap();
    assert_eq!(res.r, 3.0);
  }
}
